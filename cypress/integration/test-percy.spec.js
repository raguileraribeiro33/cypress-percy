///<reference types="cypress" />

describe('Site Duck Duck Go', () => {

    it('Realizar busca', () => {
       
        cy.visit('https://duckduckgo.com/')
    
        cy.get('.badge-link__btn').contains('Add Duck')
            .should('be.visible')
        cy.percySnapshot('Duck')

    })

    it('Acessar novo site de busca', () => {
        cy.visit('https://www.bing.com/?cc=br')
    
        cy.get('.sw_lang').contains('English')
        .should('be.visible')
        cy.percySnapshot('Bing')
        
    });


})